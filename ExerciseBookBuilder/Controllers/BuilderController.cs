﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExerciseBookBuilder.Controllers
{
    public class BuilderController : Controller
    {
        // GET: Builder
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult FrontCover()
        {
            return View();
        }
        public ActionResult Inside()
        {
            return View();
        }
        public ActionResult First()
        {
            return View();
        }
        public ActionResult Year7Cover()
        {
            return View();
        }
        public ActionResult Year7Intro()
        {
            return View();
        }
        public ActionResult Year7Assessment()
        {
            return View();
        }
        public ActionResult Year8Cover()
        {
            return View();
        }
        public ActionResult Year8Intro()
        {
            return View();
        }
        public ActionResult Year8Assessment()
        {
            return View();
        }
        public ActionResult Year9Cover()
        {
            return View();
        }
        public ActionResult Year9Intro()
        {
            return View();
        }
        public ActionResult Year9Assessment()
        {
            return View();
        }
        public ActionResult CriteriaCover()
        {
            return View();
        }
        public ActionResult CriteriaFootball()
        {
            return View();
        }
        public ActionResult CriteriaBasketball()
        {
            return View();
        }
        public ActionResult CriteriaGymnastics()
        {
            return View();
        }
        public ActionResult CriteriaHockey()
        {
            return View();
        }
        public ActionResult CriteriaRugby()
        {
            return View();
        }
        public ActionResult CriteriaTennis()
        {
            return View();
        }
        public ActionResult Back()
        {
            return View();
        }
        public ActionResult Finish()
        {
            return View();
        }

    }
}